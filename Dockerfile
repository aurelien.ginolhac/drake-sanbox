FROM rocker/verse:3.6.2

RUN apt-get update && \
  apt-get install -y --allow-downgrades \
  --allow-remove-essential --allow-change-held-packages \
  --allow-unauthenticated --no-install-recommends --no-upgrade \
  curl \
  zip \
  wkhtmltopdf && \
  strip --remove-section=.note.ABI-tag /usr/lib/x86_64-linux-gnu/libQt5Core.so.5

# https://github.com/dnschneid/crouton/wiki/Fix-error-while-loading-shared-libraries:-libQt5Core.so.5
# libQ5Core issue, hotfix

