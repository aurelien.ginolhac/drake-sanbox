#!/usr/bin/env Rscript

library(drake)

r_make()

# Save a network graph
visNetwork::visSave(r_vis_drake_graph(),
                    here::here("artifacts", "drake_graph.html"))
print(build_times(), n = 50)
#
# # Save relevant metrics in OpenMetricc format
# # See https://prometheus.io/docs/instrumenting/exposition_formats/#text-based-format
# # https://docs.gitlab.com/ee/ci/metrics_reports.html
 cat("build_time_seconds ", round(sum(build_times()$elapsed), 2),
           file = here::here("artifacts", "metrics.txt"))