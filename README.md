---
title: "sanbox"
output:
  pdf_document: default
  html_document:
    keep_md: yes
---

## Building the docs folder

To create the html files for the docs folder, use the Build -> Build All button in RStudio. This button will use the rules defined in `make.R` to produce the website in `docs`. 

To customise the button **Build All**, in RStudio, click on the file `myproject.Rproj`, then _Build Tools_ and the path to the custom script `make.R`.

![](img/custom_make.png)


`make.R` is essentially a call to `drake::r_make()` as seen below:


```r
#!/usr/bin/env Rscript

library(drake)

r_make()
```


![](img/logo-drake.png)


### drake

[`drake`](https://github.com/ropensci/drake) is a workflow manager for R

Dependencies are defined by in a `plan` and depicted below:

![](img/dag.png)

Only detected changes tell `drake` to re-run a peculiar step and its downstream dependencies. In our case, we track the `md5sum` of source files, if they change, then we need to regenerate the html/pdf/template.

All `Rmd` files are rendered using the `rmarkdown::render_site()` function to create the files in the `docs` folder with navbar, header and footer.

- Source files are organized as follows (number in file names will be use as index in the list):
    + `lectures` folder for ioslides lectures with Rmd files supposed to start with "lecture*".
    + `practicals` folder for the tutorials rendered using `unilur::tutorial_html()` and `unilur::tutorial_html_solution()`. Rmd files should start with "practical*".
    + `projects` folder for the tutorials rendered using `unilur::tutorial_html()` and `unilur::tutorial_html_answer()`. Rmd files should start with "project*".
    + Every file in these folders not starting with an underscore ("_") and not being a used source will be copied to the docs folder (this is a `rmarkdown::render_site()` behaviour). Except those listed in `exclude` in `_site.yml`


### renv

[`renv`](https://rstudio.github.io/renv/articles/renv.html) is a R package for a 
local dependency of packages. It works with a `renv.lock` JSON file that contains 
the packages used in a project, their versions, and origin (CRAN, GitHub, Gitlab, Bioconductor).

the function `renv::snapshot()` update the `renv.lock` file, which is tracked with `git`.
On a remote place, `renv::restore()` will then populate a local environment with the exact same packages set.

Of note, 2 files are necessary and must be tracked:

- `renv/activate.R` it tweaks the `libpaths` among other things
- `.Rprofile` that source the previous file and ensure to be in a local `renv` env

### gitlab Continuous Integration



